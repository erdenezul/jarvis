"""
    Author Erdenezul Batmunkh
"""
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# GPIO variables
MOTION_SENSOR = 21
LED_OUTPUT = 20
SINGLE_RELAY = 4

# delay timing variables
TURN_OFF_LIGHT_DELAY = 10


GPIO.setup(MOTION_SENSOR, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(LED_OUTPUT, GPIO.OUT)


def turn_on_light():
    """
        A function for turn on light
        send signal to relay to close electric circiut
        which means turn on light
    """
    GPIO.setup(SINGLE_RELAY, GPIO.OUT, initial=GPIO.LOW)
    GPIO.output(SINGLE_RELAY, GPIO.HIGH)


def turn_off_light():
    """
        A function for turn off light
        clear signal to relay GPIO port which means
        turn off light
    """
    GPIO.cleanup([SINGLE_RELAY])


def motion_sensor():
    """
        A function waiting signal from GPIO for
        motion sensor.
        When signal recieved, it will turn on light, led.
        It will be automatically turn off after delay.
    """
    if GPIO.input(MOTION_SENSOR):
        print "Motion detected"
        GPIO.output(LED_OUTPUT, GPIO.HIGH)
        turn_on_light()
        time.sleep(TURN_OFF_LIGHT_DELAY)
        print "now turning off"
        turn_off_light()
        GPIO.output(LED_OUTPUT, GPIO.LOW)


try:
    while True:
        motion_sensor()
finally:
    GPIO.cleanup()
    print "All cleaned up"
